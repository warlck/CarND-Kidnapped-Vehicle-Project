/*
 * particle_filter.cpp
 *
 *  Created on: Dec 12, 2016
 *      Author: Tiffany Huang
 */

#include <random>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <math.h> 
#include <iostream>
#include <sstream>
#include <string>
#include <iterator>


#include "particle_filter.h"

using namespace std;

void ParticleFilter::init(double x, double y, double theta, double std[]) {

    num_particles = 100;
    default_random_engine gen;
    double std_x, std_y, std_theta;
    std_x = std[0];
    std_y = std[1];
    std_theta = std[2];

    normal_distribution<double> dist_x(x, std_x);
    normal_distribution<double> dist_y(y, std_y);
    normal_distribution<double> dist_theta(theta, std_theta);

    for (int i = 0; i < num_particles; i++) {
        Particle p;
        p.id = i;
        p.x = dist_x(gen);
        p.y = dist_y(gen);
        p.theta = dist_theta(gen);
        p.weight = 1;

        particles.push_back(p);
        weights.push_back(1);
    }

    is_initialized = true;

}

void ParticleFilter::prediction(double delta_t, double std_pos[], double velocity, double yaw_rate) {
	//  http://en.cppreference.com/w/cpp/numeric/random/normal_distribution
	//  http://www.cplusplus.com/reference/random/default_random_engine/
    default_random_engine gen;
    double new_x, new_y, new_theta;

    for (int i = 0; i < num_particles; i++) {
        Particle p = particles[i];
        if (yaw_rate == 0) {
            new_x = p.x + velocity*delta_t*cos(p.theta);
            new_y = p.y + velocity*delta_t*sin(p.theta);
            new_theta = p.theta;
        } else {
            new_x = p.x + velocity/yaw_rate*(sin(p.theta + yaw_rate*delta_t) - sin(p.theta));
            new_y = p.y + velocity/yaw_rate*(cos(p.theta) - cos(p.theta + yaw_rate*delta_t));
            new_theta = p.theta + yaw_rate*delta_t;
        }

        normal_distribution<double> dist_x(new_x, std_pos[0]);
        normal_distribution<double> dist_y(new_y, std_pos[1]);
        normal_distribution<double> dist_theta(new_theta, std_pos[2]);

        particles[i].x = dist_x(gen);
        particles[i].y = dist_y(gen);
        particles[i].theta = dist_theta(gen);
    }

}

void ParticleFilter::dataAssociation(std::vector<LandmarkObs> predicted, std::vector<LandmarkObs>& observations) {
	// TODO: Find the predicted measurement that is closest to each observed measurement and assign the 
	//   observed measurement to this particular landmark.
	// NOTE: this method will NOT be called by the grading code. But you will probably find it useful to 
	//   implement this method and use it as a helper during the updateWeights phase.

}

void ParticleFilter::updateWeights(double sensor_range, double std_landmark[], 
		std::vector<LandmarkObs> observations, Map map_landmarks) {
	//   The following is a good resource for the theory:
	//   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
	//   and the following is a good resource for the actual equation to implement (look at equation 
	//   3.33
	//   http://planning.cs.uiuc.edu/node99.html

    for (int p = 0; p < num_particles; p++) {
        std::vector<int> associations;
        std::vector<double> sense_x;
        std::vector<double> sense_y;


        std::vector<LandmarkObs> transformed_observations;
        LandmarkObs t_o;
        LandmarkObs obs;
        Particle particle = particles[p];

        for (int j = 0; j < observations.size(); j++) {
            obs = observations[j];

            t_o.x = particle.x + obs.x*cos(particle.theta) - obs.y*sin(particle.theta);
            t_o.y = particle.y + obs.x*sin(particle.theta) + obs.y*cos(particle.theta);
            transformed_observations.push_back(t_o);
        }

        particles[p].weight = 1;

        double closest = sensor_range*sensor_range;

        for (int i = 0; i < transformed_observations.size(); i++) {
            LandmarkObs obs = transformed_observations[i];
            int minIndex = 0;

            for (int j = 0; j < map_landmarks.landmark_list.size(); j++) {
                double landmark_x = map_landmarks.landmark_list[j].x_f;
                double landmark_y = map_landmarks.landmark_list[j].y_f;

                double calculated = pow(obs.x - landmark_x, 2.0) + pow(obs.y - landmark_y, 2.0);
                if (calculated < closest) {
                    closest = calculated;
                    minIndex = j;
                }
            }

            double x = obs.x;
            double y = obs.y;

            double mu_x = map_landmarks.landmark_list[minIndex].x_f;
            double mu_y = map_landmarks.landmark_list[minIndex].y_f;

            double std_x = std_landmark[0];
            double std_y = std_landmark[1];

            double multiplier = 1/(2*M_PI*std_x*std_y)*exp(-0.5*(pow((x-mu_x)/std_x, 2) + pow((y-mu_y)/std_y, 2)));

            if (multiplier > 0) 
                particles[p].weight *= multiplier;

            associations.push_back(minIndex+1);
            sense_x.push_back(x);
            sense_y.push_back(y);

        }

        weights[p] = particles[p].weight;
        SetAssociations(particles[p], associations, sense_x, sense_y);

    }
    



}

void ParticleFilter::resample() {
	//   http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution
    std::vector<Particle> new_particles;
    default_random_engine gen;

    std::uniform_int_distribution<> int_dist(0, num_particles - 1);
    int index = int_dist(gen);

    double max_weight = *std::max_element(weights.begin(), weights.end());
    std::uniform_real_distribution<double> real_dist(0.0,2*max_weight);

    double beta = 0;
    for (int i = 0; i < num_particles; i++) {
        beta += real_dist(gen);

        while(beta > weights[index]) {
            beta -= weights[index];
            index = (index + 1) % num_particles;
        }
        new_particles.push_back(particles[index]);
    }

    particles = new_particles;


}

Particle ParticleFilter::SetAssociations(Particle particle, std::vector<int> associations, std::vector<double> sense_x, std::vector<double> sense_y)
{
	//particle: the particle to assign each listed association, and association's (x,y) world coordinates mapping to
	// associations: The landmark id that goes along with each listed association
	// sense_x: the associations x mapping already converted to world coordinates
	// sense_y: the associations y mapping already converted to world coordinates

	//Clear the previous associations
	particle.associations.clear();
	particle.sense_x.clear();
	particle.sense_y.clear();

	particle.associations= associations;
 	particle.sense_x = sense_x;
 	particle.sense_y = sense_y;

 	return particle;
}

string ParticleFilter::getAssociations(Particle best)
{
	vector<int> v = best.associations;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<int>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseX(Particle best)
{
	vector<double> v = best.sense_x;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseY(Particle best)
{
	vector<double> v = best.sense_y;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
